import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HtmlTemplateBuilder implements TemplateBuilder {
    private HashMap<String,String> parameters = new HashMap<>();
    private Logger LOGGER = Logger.getLogger(HtmlTemplateBuilder.class.getName());
    private String configFileName;
    private String outputFileName;
    private String inputFileName;
    public HtmlTemplateBuilder(String filenameIn, String filenameOut, String filenameConfig){
        inputFileName = filenameIn;
        outputFileName = filenameOut;
        configFileName = filenameConfig;
    }
    public void setConfigFileName(String param){
        configFileName = param;
    }
    public String getConfigFileName(){
        return configFileName;
    }
    public void setOutputFileName(String param){
        outputFileName = param;
    }
    public String getOutputFileName(){
        return outputFileName;
    }
    public void setInputFileName(String param){
        inputFileName = param;
    }
    public String getInputFileName(){
        return inputFileName;
    }
    public void readConfig(){
        JSONParser parser = new JSONParser();
        LOGGER.log(Level.INFO,configFileName);
        try(FileReader configFile= new FileReader(configFileName)){
            JSONObject object = (JSONObject) parser.parse(configFile);
            for( Object a: object.keySet()) {
                parameters.put(a.toString(), object.get(a).toString());
            }
        }
        catch(Exception e){
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }

    }
    public HtmlTemplate build(){
        HtmlTemplate template = new HtmlTemplate();
        readConfig();
        template.setParams(parameters);
        try {
            String htmlInput= new String( Files.readAllBytes(Paths.get(inputFileName)));
            template.setHtmlInput(htmlInput);
        }
        catch(Exception e){
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
        template.replaceVariables();
        return template;
    }
}
