public class Main {
    public static void main(String[] args) {
        DatabaseAdapter a = DatabaseAdapter.getInstance("config.json");
        System.out.println(a.toString());
        DatabaseAdapter b = DatabaseAdapter.getInstance("config.json");
        System.out.println(b.toString());
        System.out.println(a.getOne().toString());
        //test html template builder
        HtmlTemplateBuilder build = new HtmlTemplateBuilder("template.html","output.html",
                "template.json");
        HtmlTemplate template = build.build();
        System.out.println(template.getHtmlOutput());

    }
}