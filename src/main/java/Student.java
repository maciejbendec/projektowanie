public class Student {
    private String imie;
    private String nazwisko;
    private int klucz;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getKlucz() {
        return klucz;
    }

    public void setKlucz(int klucz) {
        this.klucz = klucz;
    }
    public String toString(){
        return Integer.toString(klucz)+" "+imie+" "+nazwisko;
    }
}