import java.util.*;


public interface Database
{
    Student getOne();
    List<Student> getAll();
    Student getByKey(String key);
    void insert(String imie, String nazwisko);
    void update(String imie, String nazwisko, String condition);
    void delete(String condition);
}
