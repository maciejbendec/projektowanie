import java.sql.*;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabasePostgres implements Database
{
    Logger LOGGER = Logger.getLogger(Database.class.getName());
    private static final String ID = "id" ;
    private static final String IMIE = "imie" ;
    private static final String NAZWISKO = "nazwisko";
    private Connection connection;
    public DatabasePostgres(String url) {
        try {
            connection = DriverManager.getConnection(url);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
    public Student getOne(){
        String query = "select * from Studenci";
        Student student = new Student();
        try( PreparedStatement pstmt = connection.prepareStatement(query))
        {
            try(ResultSet wyniki = pstmt.executeQuery()) {
                wyniki.next();
                student.setKlucz(wyniki.getInt(ID));
                student.setImie(wyniki.getString(IMIE));
                student.setNazwisko(wyniki.getString(NAZWISKO));
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
        return null;
    }

    public List<Student> getAll(){
        try
        {
            String query = "select Name from Studenci";
            try( PreparedStatement pstmt = connection.prepareStatement(query)){
                List<Student> set = new LinkedList<>();
                try (ResultSet wyniki = pstmt.executeQuery()) {
                    while (wyniki.next()) {
                        Student student = new Student();
                        student.setKlucz(wyniki.getInt(ID));
                        student.setImie(wyniki.getString(IMIE));
                        student.setNazwisko(wyniki.getString(NAZWISKO));
                        set.add(student);
                    }
                    return set;
                }
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
        return Collections.emptyList();
    }

    public Student getByKey(String key){
        try
        {
            String query = "select Name from Studenci WHERE id == ?";
            Student student = new Student();
            try( PreparedStatement pstmt = connection.prepareStatement(query)){
                pstmt.setString(1, key);
                try(ResultSet wyniki = pstmt.executeQuery()) {
                    student.setKlucz(wyniki.getInt(ID));
                    student.setImie(wyniki.getString(IMIE));
                    student.setNazwisko(wyniki.getString(NAZWISKO));
                }
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
        return null;
    }
    public void insert(String imie, String nazwisko){
        try
        {
            String query = "INSERT INTO Studenci (imie,nazwisko) VALUES (?,?)";
            try( PreparedStatement pstmt = connection.prepareStatement(query)){
                pstmt.setString(1, imie);
                pstmt.setString(2, nazwisko);
                pstmt.executeUpdate();
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
    public void update(String imie, String nazwisko, String condition){
        try
        {
            String query = "UPDATE Studenci SET imie = ?, nazwisko = ?  WHERE id == ?";
            try(PreparedStatement pstmt = connection.prepareStatement(query)) {
                pstmt.setString(1, imie);
                pstmt.setString(2, nazwisko);
                pstmt.setString(3, condition);
                pstmt.executeUpdate();
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
    public void delete(String condition){
        try
        {
            String query = "DELETE FROM Studenci WHERE id == ?";
            try(PreparedStatement pstmt = connection.prepareStatement(query)) {
                pstmt.setString(1, condition);
                pstmt.executeUpdate();
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
}
