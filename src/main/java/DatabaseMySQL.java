
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseMySQL implements Database {
    private static final String ID = "id" ;
    private static final String IMIE = "imie" ;
    private static final String NAZWISKO = "NAZWISKO";
    Logger LOGGER = Logger.getLogger(Database.class.getName());
    private Connection connection;
    public DatabaseMySQL(String url, String user, String password) {
        try {
            connection = DriverManager.getConnection(url,user,password);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
    public Student getOne(){
        String query = "select * from studenci";
        Student student = new Student();
        try( PreparedStatement pstmt = connection.prepareStatement(query))
        {
            try(ResultSet wyniki = pstmt.executeQuery()) {
                wyniki.next();
                student.setKlucz(wyniki.getInt(ID));
                student.setImie(wyniki.getString(IMIE));
                student.setNazwisko(wyniki.getString(NAZWISKO));
                return student;
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
        return null;
    }

    public List<Student> getAll(){
        try
        {
            String query = "select Name from studenci";
            try( PreparedStatement pstmt = connection.prepareStatement(query)){
                List<Student> set = new LinkedList<>();
                try (ResultSet wyniki = pstmt.executeQuery()) {
                    while (wyniki.next()) {
                        Student student = new Student();
                        student.setKlucz(wyniki.getInt("ID"));
                        student.setImie(wyniki.getString("IMIE"));
                        student.setNazwisko(wyniki.getString(NAZWISKO));
                        set.add(student);
                    }
                    return set;
                }
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
        return Collections.emptyList();
    }

    public Student getByKey(String key){
        try
        {
            String query = "select Name from studenci WHERE ID == ?";
            Student student = new Student();
            try( PreparedStatement pstmt = connection.prepareStatement(query)){
                pstmt.setString(1, key);
                try(ResultSet wyniki = pstmt.executeQuery()) {
                    student.setKlucz(wyniki.getInt("ID"));
                    student.setImie(wyniki.getString("IMIE"));
                    student.setNazwisko(wyniki.getString(NAZWISKO));
                }
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
        return null;
    }
    public void insert(String imie, String nazwisko){
        try
        {
            String query = "INSERT INTO studenci (IMIE,NAZWISKO) VALUES (?,?)";
            try( PreparedStatement pstmt = connection.prepareStatement(query)){
                pstmt.setString(1, imie);
                pstmt.setString(2, nazwisko);
                pstmt.executeUpdate();
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
    public void update(String imie, String nazwisko, String condition){
        try
        {
            String query = "UPDATE studenci SET IMIE = ?, NAZWISKO = ? WHERE ID == ?";
            try(PreparedStatement pstmt = connection.prepareStatement(query)) {
                pstmt.setString(1, imie);
                pstmt.setString(2, nazwisko);
                pstmt.setString(3, condition);
                pstmt.executeUpdate();
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
    public void delete(String condition){
        try
        {
            String query = "DELETE FROM studenci WHERE ID == ?";
            try(PreparedStatement pstmt = connection.prepareStatement(query)) {
                pstmt.setString(1, condition);
                pstmt.executeUpdate();
            }
        }
        catch (SQLException e)
        {
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
}
