import java.util.Map;

public class HtmlTemplate {
    public void setHtmlInput(String htmlInput) {
        this.htmlInput = htmlInput;
    }

    public String getHtmlOutput() {
        return htmlOutput;
    }

    private String htmlInput;

    public Map<String, String> getParams() {
        return params;
    }

    private Map<String,String> params;
    private String htmlOutput;

    public void setParams(Map<String, String> params) {
        this.params = params;
    }


    public void replaceVariables(){
        htmlOutput=htmlInput;
        for(Map.Entry<String,String> parameter : params.entrySet() ){
            htmlOutput= htmlOutput.replace("${"+parameter.getKey()+"}",parameter.getValue());
        }
    }

}
