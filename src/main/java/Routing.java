import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kprzystalski on 13/12/2017.
 */
public class Routing {
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final int STATUS_OK = 200;
    private static final int STATUS_NOT_FOUND = 404;
    private static final int STATUS_METHOD_NOT_ALLOWED = 405;
    static Logger LOGGER = Logger.getLogger(Routing.class.getName());
    private static final Charset CHARSET = StandardCharsets.UTF_8;
    private static Map<String,List<String>> routingTable= new HashMap<>();
    private static DatabaseAdapter database = DatabaseAdapter.getInstance("config.json");
    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(9000), 0);
        parseCsv(server);
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    static class MainHandler implements HttpHandler {
        String templateName;
        MainHandler(String templateNameParam)
        {
            templateName = templateNameParam;
        }
        @Override
        public void handle(HttpExchange he) throws IOException {
            try {
                final Headers headers = he.getResponseHeaders();
                final String uri = he.getRequestURI().getPath();
                final String requestMethod = he.getRequestMethod().toUpperCase();
                LOGGER.log(Level.INFO,requestMethod);
                if(!routingTable.containsKey(uri)){
                    new ErrorHandler(STATUS_NOT_FOUND).handle(he);
                }
                else if(!routingTable.get(uri).contains(requestMethod)){
                    new ErrorHandler(STATUS_METHOD_NOT_ALLOWED).handle(he);
                }
                else {
                    switch (requestMethod) {
                        case "GET":
                            // replace with template handler
                            if(templateName.equals("JSON")){
                                final String responseBody = "['"+requestMethod+"']";
                                headers.set(HEADER_CONTENT_TYPE, String.format("application/json; charset=%s", CHARSET));
                                final byte[] rawResponseBody = responseBody.getBytes(CHARSET);
                                he.sendResponseHeaders(STATUS_OK, rawResponseBody.length);
                                he.getResponseBody().write(rawResponseBody);
                            }
                            else{
                                HtmlTemplateBuilder build = new HtmlTemplateBuilder("template.html", "output.html",
                                        "template.json");
                                HtmlTemplate template = build.build();
                                Map<String, String> parameters = template.getParams();
                                parameters.remove("zmienna1");
                                parameters.put("zmienna1", "hello");
                                template.setParams(parameters);
                                template.replaceVariables();
                                final String response = template.getHtmlOutput();
                                he.sendResponseHeaders(STATUS_OK, response.length());
                                OutputStream os = he.getResponseBody();
                                os.write(response.getBytes());
                                os.close();
                            }
                            break;
                            case "PUT":
                            case "DELETE":
                            case "POST":
                                new JSONRequestHandler().handle(he);
                                break;
                            default:
                                new ErrorHandler(STATUS_METHOD_NOT_ALLOWED).handle(he);
                                break;
                        }
                }
            } finally {
                he.close();
            }
        }
    }
    static class JSONRequestHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange he) throws IOException {
            final String requestMethod = he.getRequestMethod().toUpperCase();
            final Headers headers = he.getResponseHeaders();
            LOGGER.log(Level.INFO,requestMethod);
            switch (requestMethod) {
                case "PUT":
                database.update("Maciej","Bendec","1");
                break;
                case "DELETE":
                database.delete("1");
                break;
                case "POST":
                database.insert("Maciej","Bendec");
                break;
                default:
                break;
            }
            final String responseBody = "['"+requestMethod+"']";
            headers.set(HEADER_CONTENT_TYPE, String.format("application/json; charset=%s", CHARSET));
            final byte[] rawResponseBody = responseBody.getBytes(CHARSET);
            he.sendResponseHeaders(STATUS_OK, rawResponseBody.length);
            he.getResponseBody().write(rawResponseBody);
            OutputStream os = he.getResponseBody();
            os.write(rawResponseBody);
            os.close();
        }
    }
    static class ErrorHandler implements HttpHandler {
        private int errorCode = 0;
        ErrorHandler(int error){
            errorCode = error;
        }
        @Override
        public void handle(HttpExchange he) throws IOException {
            LOGGER.log(Level.INFO,"Error handled");
            HtmlTemplateBuilder build = new HtmlTemplateBuilder("templateError.html","output.html",
                    "templateError.json");
            HtmlTemplate template = build.build();
            Map<String,String> parameters = template.getParams();
            parameters.remove("error");
            parameters.put("error",String.valueOf(errorCode));
            template.setParams(parameters);
            template.replaceVariables();
            final String response = template.getHtmlOutput();
            LOGGER.log(Level.INFO,response);
            he.sendResponseHeaders(errorCode, response.length());
            OutputStream os = he.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    private static void parseCsv(HttpServer server){
        String temp;
        server.createContext("/", new ErrorHandler(STATUS_NOT_FOUND));
        try(BufferedReader routingFile= new BufferedReader(new FileReader("Routing.csv"))) {
            while((temp=routingFile.readLine())!=null){
                String[] a=temp.split(",");
                if(routingTable.containsKey(a[0])){
                    routingTable.get(a[0]).add(a[1]);
                }
                else {
                    server.createContext(a[0], new MainHandler(a[2]));
                    routingTable.put(a[0],new LinkedList<>());
                    routingTable.get(a[0]).add(a[1]);
                }
            }
        }
        catch(Exception e){
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }
    }
}