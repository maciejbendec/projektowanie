interface TemplateBuilder {
    void setConfigFileName(String param);
    String getConfigFileName();
    void setOutputFileName(String param);
    String getOutputFileName();
    void setInputFileName(String param);
    String getInputFileName();
    void readConfig();
}
