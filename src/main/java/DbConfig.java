import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbConfig {
    Logger LOGGER = Logger.getLogger(DbConfig.class.getName());
    private String PARAM_URL = "url";
    private String PARAM_PORT = "port";
    private String PARAM_USERNAME = "username";
    private String PARAM_PASSWORD = "password"; //NOSONAR this is parameter name not a real password
    private String PARAM_DATABASE = "database";
    private String PARAM_TYPE = "type";
    private HashMap<String,String> parameters = new HashMap<>();
    public void readConfig(String configFileName){
        JSONParser parser = new JSONParser();
        try(FileReader configFile= new FileReader(configFileName)){
            JSONObject object = (JSONObject) parser.parse(configFile);
            parameters.put(PARAM_URL,object.get(PARAM_URL).toString());
            parameters.put(PARAM_PORT,object.get(PARAM_PORT).toString());
            parameters.put(PARAM_USERNAME,object.get(PARAM_USERNAME).toString());
            parameters.put(PARAM_PASSWORD,object.get(PARAM_PASSWORD).toString());
            parameters.put(PARAM_DATABASE,object.get(PARAM_DATABASE).toString());
            parameters.put(PARAM_TYPE,object.get(PARAM_TYPE).toString());
        }
        catch(Exception e){
            LOGGER.log(Level.SEVERE,e.toString(),e);
        }

    }
    public String getType(){
        return parameters.get(PARAM_TYPE);
    }
    public String getUrl(){
        return parameters.get(PARAM_URL);
    }
    public String getUser(){
        return parameters.get(PARAM_USERNAME);
    }
    public String getPassword(){
        return parameters.get(PARAM_PASSWORD);
    }
    public String getDb(){
        return parameters.get(PARAM_DATABASE);
    }
    public String getPort(){
        return parameters.get(PARAM_PORT);
    }
}
