import java.util.List;

public class DatabaseAdapter implements Database {
    private static Database db;
    private static DatabaseAdapter instance = null;
    private DatabaseAdapter(){

    }
    public static DatabaseAdapter getInstance(String configFileName) {
        if (instance == null) {
            DbConfig parameters= new DbConfig();
            instance = new DatabaseAdapter();
            parameters.readConfig(configFileName);
            if(parameters.getType().equals("MySQL")) {
                db = new DatabaseMySQL(parameters.getUrl(),parameters.getUser(),parameters.getPassword());
            }
            else if(parameters.getType().equals("sqlite")) {
                db = new DatabaseSQLite(parameters.getUrl());
            }
            else if(parameters.getType().equals("postgres")) {
                db = new DatabasePostgres(parameters.getUrl());
            }
        }
        return instance;
    }
    public Student getOne(){
        return db.getOne();
    }
    public List<Student> getAll()
    {
        return db.getAll();
    }
    public Student getByKey(String key)
    {
        return db.getByKey(key);
    }
    public void insert(String imie, String nazwisko)
    {
        db.insert(imie,nazwisko);
    }
    public void update(String imie, String nazwisko, String condition){
        db.update(imie,nazwisko, condition);
    }
    public void delete(String condition){
        db.delete(condition);
    }
}
